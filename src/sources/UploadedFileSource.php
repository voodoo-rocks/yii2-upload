<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 03:12
 */

namespace vr\upload\sources;

use vr\upload\Mediator;
use yii\web\UploadedFile;

/**
 * Class UploadedFileSource
 * @package vr\upload\sources
 */
class UploadedFileSource extends BinarySource
{
    /** @var UploadedFile */
    public $uploaded = null;

    /**
     * @return Mediator
     */
    public function createMediator()
    {
        return new Mediator([
            'filename'         => $this->uploaded->tempName,
            'extension'        => $this->uploaded->extension,
            'unlinkOnDestruct' => false,
        ]);
    }
}