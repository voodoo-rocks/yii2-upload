<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 21:31
 */

namespace vr\upload\sources;

use vr\upload\Mediator;
use yii\base\InvalidArgumentException;

/**
 * Class Base64Source
 * @package vr\upload\sources
 */
class Base64Source extends BinarySource
{
    const STOPPER = 'base64,';

    /**
     * @var
     */
    public $data;

    /**
     * @var
     */
    private $filename;

    /**
     * @return Mediator
     * @throws InvalidArgumentException
     */
    public function createMediator()
    {
        $this->filename = $this->tempFilename;

        if (($pos = strpos($this->data, self::STOPPER)) !== false) {
            $this->data = substr($this->data, $pos + strlen(self::STOPPER));
        }

        file_put_contents($this->filename, base64_decode($this->data), LOCK_EX);

        return new Mediator([
            'filename' => $this->filename,
        ]);
    }
}