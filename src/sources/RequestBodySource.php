<?php


namespace vr\upload\sources;

use vr\upload\Mediator;
use yii\base\InvalidArgumentException;

/**
 * Class RequestBodySource
 * @package vr\upload\sources
 */
class RequestBodySource extends BinarySource
{
    /**
     * @var
     */
    private $filename;

    /**
     * @return Mediator
     * @throws InvalidArgumentException
     */
    public function createMediator()
    {
        $this->filename = $this->tempFilename;

        file_put_contents($this->filename, \Yii::$app->request->rawBody, LOCK_EX);

        return new Mediator([
            'filename' => $this->filename,
        ]);
    }
}