<?php

namespace vr\upload\sources;

use Yii;
use yii\base\BaseObject;

/**
 * Class BinarySource
 * @package vr\upload\sources
 * @property string $tempFilename
 */
abstract class BinarySource extends BaseObject
{
    /**
     * @return mixed
     */
    abstract public function createMediator();

    /**
     * @return bool
     */
    public function validate()
    {
        // TODO: for future use
        return true;
    }

    /**
     * @return string
     */
    public function getTempFilename(): string
    {
        return Yii::getAlias('@runtime/') . md5(uniqid());
    }
}