<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 08/01/2017
 * Time: 02:34
 */

namespace vr\upload\sources;

use vr\upload\Mediator;
use yii\base\InvalidArgumentException;
use yii\web\NotFoundHttpException;

/**
 * Class UriSource
 * @package vr\upload\sources
 */
class UriSource extends BinarySource
{
    /**
     * @var
     */
    public $uri;

    /**
     * @var
     */
    private $filename;

    /**
     * @return Mediator
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function createMediator()
    {
        if (!$this->checkAvailability($this->uri)) {
            throw new NotFoundHttpException('Resource not found ' . $this->uri);
        };

        $this->filename = $this->tempFilename;
        $content        = file_get_contents($this->uri);

        file_put_contents($this->filename, $content, LOCK_EX);

        return new Mediator([
            'filename' => $this->filename,
        ]);
    }

    private function checkAvailability($uri)
    {
        $isUrl = (bool)filter_var($uri, FILTER_VALIDATE_URL);
        $valid = $isUrl ? (bool)curl_init($uri) : file_exists($uri);

        return $valid;
    }
}