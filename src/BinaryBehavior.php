<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 01:47
 */

namespace vr\upload;

use ReflectionException;
use vr\upload\sources\BinarySource;
use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Class BinaryBehavior
 * @package vr\upload
 * Usage:
 *  1. Add this to your model class
 *  public function behaviors()
 *  {
 *      return [
 *          [
 *              'class' => BinaryBehavior::class,
 *              'binaryAttributes' => [
 *                  'binary' => [
 *                      'template' => '{base}-{datetime}-{tag}',
 *                      'connector' => [
 *                          'class' => FileSystemDataConnector::class,
 *                      ],
 *                  ]
 *              ],
 *          ],
 *      ];
 *  }
 *  2. Don't forget to add ActiveBinaryTrait to your class to define missing functions
 *  3. Add this code to the model where you upload your binary
 *      if (($instance = UploadedFile::getInstance($this, 'binary'))) {
 *          $product->upload('binary', new UploadedFileSource([
 *              'uploaded' => $instance
 *          ]));
 *      }
 */
class BinaryBehavior extends Behavior
{
    /**
     * @var
     */
    public $binaryAttributes;

    /**
     * @var
     */
    public $descriptors;

    /**
     * @var bool
     */
    public $skipUpdateOnClean = !YII_DEBUG;

    /**
     * @var string
     */
    public $descriptorClass = AttributeDescriptor::class;

    /**
     * @var array
     */
    private $_changeQueue = [];

    /**
     * @var Mediator
     */
    private $_mediator;

    /**
     * @return array
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_INIT          => 'onInit',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeUpdate',
            BaseActiveRecord::EVENT_AFTER_UPDATE  => 'onAfterUpdate',
            BaseActiveRecord::EVENT_AFTER_INSERT  => 'onAfterInsert',
            BaseActiveRecord::EVENT_AFTER_DELETE  => 'onAfterDelete',
        ];
    }

    /**
     * Returns the qualified URI of the binary
     *
     * @param      $attribute
     *
     * @return mixed|null|string URI of the binary
     */
    public function url($attribute)
    {
        /** @var AttributeDescriptor $descriptor */
        $descriptor = $this->getDescriptor($attribute);

        return $descriptor->url;
    }

    /**
     * @param $attribute
     *
     * @return AttributeDescriptor
     */
    protected function getDescriptor($attribute)
    {
        return $this->descriptors[$attribute];
    }

    /**
     * @param string $attribute
     * @param BinarySource $source
     * @param array $options
     *  Following options are supported:
     *  - extension. If set and the web service cannot determine the extension automatically based on the
     *  binary content this extension will be used. Otherwise it will be ignored. It can be used for some content
     *  files which are not described properly in the web service configuration files.
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function upload($attribute, $source, $options = [])
    {
        if (!$source || !$source->validate()) {
            return false;
        }

        $this->_mediator = $source->createMediator();
        $this->_mediator->setOptions($options);

        $descriptor = $this->getDescriptor($attribute);

        $this->_changeQueue[] = $attribute;
        array_unique($this->_changeQueue);

        return $descriptor->upload($this->_mediator);
    }

    /**
     * @param bool $simulate
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function repair($simulate = false)
    {
        $result = true;

        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $descriptor) {
            $result &= $descriptor->repair($simulate);
        }

        return $result;
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function cleanUp()
    {
        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $descriptor) {
            $descriptor->cleanUp();
        }
    }

    /**
     * @param string $attribute
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function drop(string $attribute)
    {
        /** @var AttributeDescriptor $descriptor */
        $descriptor = $this->getDescriptor($attribute);
        $descriptor->drop();
    }

    /**
     * @throws InvalidConfigException
     */
    public function onInit()
    {
        if (!is_array($this->binaryAttributes)) {
            $this->binaryAttributes = [$this->binaryAttributes];
        }

        foreach ($this->binaryAttributes as $attribute => $params) {

            if (is_numeric($attribute)) {
                $attribute = $params;
                $params    = [];
            }

            /** @var AttributeDescriptor $descriptor */
            $descriptor = Yii::createObject($params + [
                    'class'     => $this->descriptorClass,
                    'attribute' => $attribute,
                    'model'     => $this->owner,
                ]
            );

            $this->descriptors[$attribute] = $descriptor;
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function onBeforeUpdate()
    {
        /** @var ActiveRecord $activeRecord */
        $activeRecord = $this->owner;

        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $attribute => $descriptor) {
            $dirtyAttributes = $activeRecord->getDirtyAttributes($descriptor->getBaseAttributes());

            if (count($dirtyAttributes)) {
                $this->_changeQueue[] = $attribute;
            }

            $descriptor->onBeforeUpdate();

            $changed = $activeRecord->getDirtyAttributes([$attribute]);
            if (count($changed) && !$changed[$attribute]) {
                $descriptor->drop($activeRecord->getOldAttribute($attribute));
            }
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function onAfterUpdate()
    {
        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $attribute => $descriptor) {
            if (in_array($attribute, $this->_changeQueue)) {
                $descriptor->onAfterUpdate();
            }
        }
    }

    /**
     *
     */
    public function onAfterInsert()
    {
        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $attribute => $descriptor) {
            if (in_array($attribute, $this->_changeQueue)) {
                $descriptor->onAfterInsert();
            }
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function onAfterDelete()
    {
        /** @var AttributeDescriptor $descriptor */
        foreach ($this->descriptors as $descriptor) {
            $descriptor->onAfterDelete();
        }
    }
}