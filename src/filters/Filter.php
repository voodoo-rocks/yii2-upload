<?php

namespace vr\upload\filters;

use vr\upload\Mediator;
use yii\base\BaseObject;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:13
 */
abstract class Filter extends BaseObject
{
    /**
     * @param Mediator $mediator
     *
     * @return bool
     */
    abstract public function apply($mediator);
}