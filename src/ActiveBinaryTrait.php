<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 01:30
 */

namespace vr\upload;

use ReflectionException;
use vr\upload\sources\BinarySource;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;

/**
 * Class ActiveBinaryTrait
 * @package vr\upload
 */
trait ActiveBinaryTrait
{
    /**
     * @param bool $simulate
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function repair($simulate = false)
    {
        return $this->getBehaviour()->repair($simulate);
    }

    /**
     * @return BinaryBehavior
     */
    private function getBehaviour()
    {
        /** @var Component $component */
        $component = $this;

        $component->ensureBehaviors();

        foreach ($component->behaviors as $behavior) {
            if (is_a($behavior, BinaryBehavior::class)) {

                /** @noinspection PhpIncompatibleReturnTypeInspection */
                return $behavior;
            }
        }

        return null;
    }

    /**
     * @param string $attribute
     *
     * @return mixed|null|string
     */
    public function url($attribute)
    {
        return $this->getBehaviour()->url($attribute);
    }

    /**
     * @param string $attribute
     * @param BinarySource $source
     * @param array $options
     *  Following options are supported:
     *  - defaultExtension. If set and the web service cannot determine the extension automatically based on the
     *  binary content this extension will be used. Otherwise it will be ignored. It can be used for some content
     *  files which are not described properly in the web service configuration files.
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function upload($attribute, $source, $options = [])
    {
        return $this->getBehaviour()->upload($attribute, $source, $options);
    }
}