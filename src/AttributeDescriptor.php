<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:16
 */

namespace vr\upload;

use DateTime;
use ReflectionClass;
use ReflectionException;
use Throwable;
use vr\upload\connectors\DataConnector;
use vr\upload\sources\UriSource;
use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\validators\UrlValidator;

/**
 * Class AttributeDescriptor
 * @package vr\upload
 * @property array $baseAttributes
 * @property string $basename
 * @property BaseActiveRecord $model
 * @property string $url
 */
class AttributeDescriptor extends BaseObject
{
    /**
     * @var array
     */
    public $filters = [];

    /**
     * @var
     */
    public $attribute;

    /**
     * @var array
     */
    public $connector = '\vr\upload\connectors\FileSystemDataConnector';

    /**
     * @var string
     */
    public $template = '{base}-{tag}';

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var
     */
    private $_dirtyValue;

    /**
     * @return mixed|string
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function getUrl()
    {
        $filename = $this->model->{$this->attribute};

        if ((new UrlValidator())->validate($filename)) {
            return $filename;
        }

        return $this->getConnector()->url($filename);
    }

    /**
     * @return DataConnector
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    protected function getConnector()
    {
        $class = (new ReflectionClass($this->model))->getShortName();

        /** @var DataConnector $instance */
        $instance = Yii::createObject($this->connector + [
                'folder' => Inflector::camel2id($class, '-'),
            ]
        );

        return $instance;
    }

    /**
     * @param bool $simulate
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function repair($simulate = false)
    {
        $filename = $this->model->{$this->attribute};

        if (!$this->validateFile($filename)) {
            return false;
        }

        if ($simulate) {
            return true;
        }

        $source = new UriSource([
            'uri' => $this->getConnector()->locate($filename),
        ]);

        $mediator = $source->createMediator();

        return $this->upload($mediator);
    }

    /**
     * @param $filename
     *
     * @return bool
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    protected function validateFile($filename)
    {
        if (empty($filename)) {
            return false;
        }

        $connector = $this->getConnector();

        if (!$connector->exists($filename)) {
            $path = $connector->locate($filename);

            $this->model->addError($this->attribute, "File {$path} was not found");
            $this->model->{$this->attribute} = null;

            return false;
        }

        return true;
    }

    /**
     * @param Mediator $mediator
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function upload(Mediator $mediator)
    {
        foreach ($this->filters as $name => $filter) {
            try {
                Yii::createObject($filter)->apply($mediator);
            } catch (Throwable $throwable) {
                Yii::error($throwable->getMessage());
            }
        }

        $connector = $this->getConnector();

        $filename = $this->getFilename($mediator->extension);

        if (($existing = $this->model->{$this->attribute})) {
            $connector->drop($existing);
        }

        if (!$connector->upload($mediator, $filename)) {
            $this->model->addError($this->attribute, $connector->lastError);

            return false;
        }

        $this->model->{$this->attribute} = $filename;

        return true;
    }

    /**
     * @param $extension
     *
     * @return string
     * @throws Exception
     */
    protected function getFilename($extension = null)
    {

        if (empty($extension)) {
            $previous  = ArrayHelper::getValue($this->model, $this->attribute);
            $extension = pathinfo($previous, PATHINFO_EXTENSION);
        }

        return $this->getBasename() . ($extension ? '.' . $extension : null);
    }

    /**
     * @param bool $tagIncluded
     *
     * @return mixed|string
     * @throws Exception
     */
    protected function getBasename($tagIncluded = true)
    {
        $datetime = new DateTime();

        $replacements = [];

        foreach ($this->getBaseAttributes() as $attribute) {
            $value = ArrayHelper::getValue($this->model, $attribute);
            $value = preg_replace('/[^A-Za-z0-9]/', '-', $value);

            $replacements["{attribute:$attribute}"] = $value;
        }

        $replacements += [
            '{tag}'      => $tagIncluded ? Yii::$app->security->generateRandomString(8) : null,
            '{datetime}' => $datetime->format('Y-m-d-H-i-s'),
            '{time}'     => $datetime->format('H-i-s'),
            '{date}'     => $datetime->format('Y-m-d'),
            '{base}'     => $this->attribute,
        ];

        $basename = strtr($this->template, $replacements);

        return Inflector::slug($basename);
    }

    /**
     * @return array|mixed
     */
    public function getBaseAttributes()
    {
        preg_match_all('/{attribute:(.*?)}/', $this->template, $matches);

        $attributes = ArrayHelper::getValue($matches, 1);

        if (!$attributes || !is_array($attributes)) {
            $attributes = [];
        }

        return $attributes;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function cleanUp()
    {
        $this->getConnector()->cleanUp($this->model->{$this->attribute});
    }

    /**
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function onAfterDelete()
    {
        $this->drop();
    }

    /**
     * @param null $filename
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function drop($filename = null)
    {
        $filename = $filename ?: $this->model->{$this->attribute};

        if ($filename) {
            $this->getConnector()->drop($filename);

            $this->model->{$this->attribute} = null;
        }
    }

    /**
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function onAfterUpdate()
    {
        $connector = $this->getConnector();
        $source    = $this->_dirtyValue;

        if ($source) {
            $connector->rename($source, $this->model->{$this->attribute});
        }
    }

    /**
     */
    public function onBeforeUpdate()
    {
        $this->_dirtyValue = $this->model->getOldAttribute($this->attribute);
    }

    /**
     */
    public function onAfterInsert()
    {
    }
}