<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:12
 */

namespace vr\upload;

use Exception;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class Mediator
 * @package vr\upload
 * @property mixed $options
 * @property string extension
 */
class Mediator extends BaseObject
{
    /**
     * @var string
     */
    public $filename;

    /**
     * @var bool
     */
    public $unlinkOnDestruct = true;

    /**
     * @var
     */
    public $extension;

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->setExtension($this->extension ?: $this->detectExtension());
    }

    /**
     * @param $value
     */
    public function setExtension($value)
    {
        $this->extension = $value;

        $previous       = $this->filename;

        $dir      = pathinfo($previous, PATHINFO_DIRNAME);
        $filename = pathinfo($previous, PATHINFO_FILENAME);

        $this->filename = "{$dir}/{$filename}.{$value}";

        rename($previous, $this->filename);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    private function detectExtension()
    {
        $mime       = FileHelper::getMimeType($this->filename);
        $extensions = FileHelper::getExtensionsByMimeType($mime);
        $extension  = ArrayHelper::getValue($extensions, max(count($extensions) - 1, 0));

        return trim($extension);
    }

    /**
     *
     */
    function __destruct()
    {
        $this->cleanUp();
    }

    /**
     *
     */
    public function cleanUp()
    {
        if ($this->unlinkOnDestruct) {
            try {
                unlink($this->filename);
            } catch (Exception $exception) {
            }
        }
    }

    /**
     * @param $options
     */
    public function setOptions($options)
    {
        if (($value = ArrayHelper::getValue($options, 'extension')) !== null) {
            $this->setExtension($value);
        };
    }
}