<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:43
 */

namespace vr\upload\connectors;

use vr\upload\Mediator;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * Class FileSystemDataConnector
 * @package vr\upload\connectors
 */
class FileSystemDataConnector extends DataConnector
{
    /**
     * @var string
     */
    public $uploadUrl = '@web/uploads';

    /**
     * @var bool
     */
    public $protocol = true;

    /**
     * @var string
     */
    private $uploadPath = '@webroot/uploads';

    /**
     * @param Mediator $mediator
     * @param          $filename
     *
     * @return bool
     * @throws Exception
     */
    public function upload($mediator, $filename)
    {
        if (!$this->drop($filename)) {
            return false;
        }

        $absolute = $this->locate($filename);

        if (!copy($mediator->getFilename(), $absolute)) {
            $this->lastError = ArrayHelper::getValue(error_get_last(), 'message');

            return false;
        }

        return true;
    }

    /**
     * @param $filename
     *
     * @return bool
     * @throws Exception
     */
    public function drop($filename)
    {
        $absolute = $this->locate($filename);

        if (file_exists($absolute)) {
            try {
                unlink($absolute);
            } catch (\Exception $exception) {
                $this->lastError = $exception->getMessage();

                return false;
            }
        }

        return $this->cleanUp($filename);
    }

    /**
     * @param $filename
     *
     * @param bool $fullPath
     * @return bool|string
     * @throws Exception
     */
    public function locate($filename, bool $fullPath = false)
    {
        $directory = Yii::getAlias($this->uploadPath) . '/' . $this->folder;

        if (!file_exists($directory) && !FileHelper::createDirectory($directory)) {
            $this->lastError = ArrayHelper::getValue(error_get_last(), 'message');

            return false;
        }

        return $directory . '/' . $filename;
    }

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function cleanUp($filename)
    {
        $directory = Yii::getAlias($this->uploadPath) . '/' . $this->folder;
        $mask      = pathinfo($filename, PATHINFO_FILENAME);

        foreach (FileHelper::findFiles($directory, ['only' => ["{$mask}-*"]]) as $file) {
            try {
                unlink($file);
            } catch (\Exception $exception) {
                $this->lastError = $exception->getMessage();

                return false;
            }
        }

        return true;
    }

    /**
     * @param      $filename
     *
     * @return string
     */
    public function url($filename)
    {
        if (!$filename) {
            return null;
        }

        return Url::to(Yii::getAlias($this->uploadUrl)
            . '/' . $this->folder
            . '/' . $filename, $this->protocol);
    }

    /**
     * @param string $source
     * @param string $destination
     *
     * @return bool
     * @throws Exception
     */
    public function rename($source, $destination)
    {
        if (!$this->exists($source)) {
            return false;
        }

        $source      = $this->locate($source);
        $destination = $this->locate($destination);

        if (file_exists($source) && !rename($source, $destination)) {
            $this->lastError = ArrayHelper::getValue(error_get_last(), 'message');

            return false;
        }

        return file_exists($source) && $this->cleanUp($source);
    }

    /**
     * @param $filename
     *
     * @return bool
     * @throws Exception
     */
    public function exists($filename)
    {
        return file_exists($this->locate($filename));
    }
}